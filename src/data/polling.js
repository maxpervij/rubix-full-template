let polling = {
    "T1":[
        [
            {
                date: "12/05/2016",
                bid: 1.11410,
                ask: 1.11453,
                instrument: "EUR/USD",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.927,
                        ask: 119.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.934,
                        ask: 119.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.935,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.947,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.957,
                        ask: 119.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.930,
                        ask: 119.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 1.11410,
                ask: 1.11453,
                instrument: "USD/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.927,
                        ask: 119.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.934,
                        ask: 119.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.935,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.947,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.957,
                        ask: 119.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.930,
                        ask: 119.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 1.11410,
                ask: 1.11453,
                instrument: "EUR/JPY",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.927,
                        ask: 119.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.934,
                        ask: 119.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.935,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.947,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.957,
                        ask: 119.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.930,
                        ask: 119.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 1.11410,
                ask: 1.11453,
                instrument: "JPY/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.927,
                        ask: 119.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.934,
                        ask: 119.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.935,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.947,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.957,
                        ask: 119.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.930,
                        ask: 119.967
                    }
                }
            },

        ],
        [
            {
                date: "12/05/2016",
                bid: 1.11411,
                ask: 1.11452,
                instrument: "EUR/USD",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 1.11411,
                ask: 1.11452,
                instrument: "USD/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 1.11411,
                ask: 1.11452,
                instrument: "EUR/JPY",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 1.11411,
                ask: 1.11452,
                instrument: "JPY/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 119.937,
                        ask: 119.957
                    }
                }
            },
        ]
    ],
    "T2":[
        [
            {
                date: "12/05/2016",
                bid: 2.11410,
                ask: 2.11453,
                instrument: "EUR/USD",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.927,
                        ask: 120.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.934,
                        ask: 120.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.935,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.947,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.957,
                        ask: 120.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.930,
                        ask: 120.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 2.11410,
                ask: 2.11453,
                instrument: "USD/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.927,
                        ask: 120.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.934,
                        ask: 120.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.935,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.947,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.957,
                        ask: 120.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.930,
                        ask: 120.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 2.11410,
                ask: 2.11453,
                instrument: "EUR/JPY",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.927,
                        ask: 120.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.934,
                        ask: 120.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.935,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.947,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.957,
                        ask: 120.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.930,
                        ask: 120.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 2.11410,
                ask: 2.11453,
                instrument: "JPY/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.927,
                        ask: 120.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.934,
                        ask: 120.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.935,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.947,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.957,
                        ask: 120.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.930,
                        ask: 120.967
                    }
                }
            },

        ],
        [
            {
                date: "12/05/2016",
                bid: 2.11411,
                ask: 2.11452,
                instrument: "EUR/USD",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 2.11411,
                ask: 2.11452,
                instrument: "USD/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 2.11411,
                ask: 2.11452,
                instrument: "EUR/JPY",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 2.11411,
                ask: 2.11452,
                instrument: "JPY/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 120.937,
                        ask: 120.957
                    }
                }
            },
        ]
    ],
    "T3":[
        [
            {
                date: "12/05/2016",
                bid: 3.11410,
                ask: 3.11453,
                instrument: "EUR/USD",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.927,
                        ask: 130.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.934,
                        ask: 130.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.935,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.947,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.957,
                        ask: 130.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.930,
                        ask: 130.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 3.11410,
                ask: 3.11453,
                instrument: "USD/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.927,
                        ask: 130.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.934,
                        ask: 130.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.935,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.947,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.957,
                        ask: 130.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.930,
                        ask: 130.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 3.11410,
                ask: 3.11453,
                instrument: "EUR/JPY",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.927,
                        ask: 130.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.934,
                        ask: 130.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.935,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.947,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.957,
                        ask: 130.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.930,
                        ask: 130.967
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 3.11410,
                ask: 3.11453,
                instrument: "JPY/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.927,
                        ask: 130.967
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.934,
                        ask: 130.953
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.952
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.935,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.947,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.957,
                        ask: 130.967
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.930,
                        ask: 130.967
                    }
                }
            },

        ],
        [
            {
                date: "12/05/2016",
                bid: 3.11411,
                ask: 3.11452,
                instrument: "EUR/USD",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 3.11411,
                ask: 3.11452,
                instrument: "USD/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 3.11411,
                ask: 3.11452,
                instrument: "EUR/JPY",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    }
                }
            },
            {
                date: "12/05/2016",
                bid: 3.11411,
                ask: 3.11452,
                instrument: "JPY/EUR",
                tenors: {
                    "1W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "4W": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "1M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "2M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    },
                    "3M": {
                        date: "01/01/2015",
                        title: "T1",
                        bid: 130.937,
                        ask: 130.957
                    }
                }
            },
        ]
    ],
}

export default polling;