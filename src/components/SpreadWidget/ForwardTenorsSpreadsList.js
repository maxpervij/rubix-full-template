import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

import Spot from './Spot.js'

ForwardTenorsSpreadsList.propTypes = {
    tenors: React.PropTypes.object,
    onQuote: React.PropTypes.func
};

function handleSpotClick(f,data){
    return ()=>f(data);
}

function ForwardTenorsSpreadsList(props) {
    let tenorsKeys = Object.keys(props.tenors);



    let tenorsSpreadList = tenorsKeys.map((key)=> {
        let tenor = props.tenors[key];
        return (
            <div className="row relative" key={`tenor-spread-${key}`}>
                <div className="col-xs-5">
                    <div className="spread-widget-tenors-spot">
                        <Spot rate={tenor.bid} decimal={3}
                              onClick={handleSpotClick(props.onQuote,{rate:tenor.bid, tenor:tenor.title, date:tenor.date})}/>
                    </div>
                </div>
                <div className="col-xs-offset-2 col-xs-5">
                    <div className="spread-widget-tenors-spot">
                        <Spot rate={tenor.ask} decimal={3}
                              onClick={handleSpotClick(props.onQuote,{rate:tenor.ask, tenor:tenor.title, date:tenor.date})}/>
                    </div>
                </div>
                <div className="spread-widget-tenors-spred-box centered align-center ">
                    <p><span>{key}</span></p>

                    <p><span>{tenor.date}</span></p>
                </div>
            </div>
        )
    });

    if (tenorsKeys.length > 5) {
        return (
            <Scrollbars style={{ height: "200px"}}>
                <div className="spread-widget-tenors-body container-fluid">
                    <div className="spread-widget-tenors-spreads-list">
                        {tenorsSpreadList}
                    </div>
                </div>
            </Scrollbars>
        )
    } else {
        return (
            <div className="spread-widget-tenors-body container-fluid">
                <div className="spread-widget-tenors-spreads-list">
                    {tenorsSpreadList}
                </div>
            </div>
        )
    }
}

export default ForwardTenorsSpreadsList;