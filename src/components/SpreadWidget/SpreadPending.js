import React from 'react';

SpreadPending.propTypes = {
    pending: React.PropTypes.bool
};
let pendingProgressStyle = {
    position: '',
    margin: ''
}
function SpreadPending(props) {
    if (props.pending) {
        return (
            <div className="spread-widget-pending">
                <div>
                    <div style={pendingProgressStyle}
                                      className="spread-widget-pending-progress"/>
                </div>
            </div>
        )
    } else {
        return null;
    }
}

export default SpreadPending;