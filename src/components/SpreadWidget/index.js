import React from 'react';
import PureRenderMixin from "react-addons-pure-render-mixin"

import CurrencySelectField from './CurrencySelectField.js'
import ForwardTenors from './ForwardTenors.js'
import SpreadPending from './SpreadPending.js'
import Spot from './Spot.js'

function decimalPlaces(value) {
    return value.toString().split('.')[1].length;

}
function calcSpread(bid, ask) {
    let exponent = Math.max(decimalPlaces(bid), decimalPlaces(ask)) - 1;

    return Math.round(((ask - bid) * Math.pow(10, exponent)) * 10) / 10;
}

export default React.createClass({
    propTypes: {
        date: React.PropTypes.string,
        bid: React.PropTypes.number,
        ask: React.PropTypes.number,
        instrument: React.PropTypes.string,
        tenors: React.PropTypes.object,
        onTenorsToggle: React.PropTypes.func,
        onGetQuote: React.PropTypes.func
    },
    getInitialState(){
        return {
            currency: this.props.instrument.split("/")[0],
            nominal: 10000
        };
    },
    mixins:[PureRenderMixin],
    getQuote(data){
        this.setState({
            pending: true
        });

        this.props.onGetQuote(Object.assign({}, data, {
            instrument: this.props.instrument,
            currency: this.state.currency,
            nominal: this.state.nominal
        }))
            .then(()=> {
                this.setState({
                    pending: false
                })
            })
    },
    handleNominalChange(event){
        this.setState({
            nominal: event.target.value
        })

    },
    handleCurrencyChange(event, index, value){
        this.setState({
            currency: value
        })
    },
    handleSpotClick(data){
        return ()=>this.getQuote(data);
    },

    render() {
        return (
            <div className="widget spread-widget" style={Object.assign({},{position: "relative"},this.props.style)}>
                <header><h4>{this.props.instrument}</h4></header>
                <div className="spread-widget-body container-fluid">
                    <div className="form-group-xs row-xs">
                        <div className="col-xs-8">
                            <input className="form-control input-transparent number-spin-hidden"
                                   name="nominal" style={{width: "100%"}} type="number"
                                   value={this.state.nominal} onChange={this.handleNominalChange}/>
                        </div>
                        <div className="col-xs-4">
                            <CurrencySelectField instrument={this.props.instrument} selected={this.state.currency}
                                                 onChange={this.handleCurrencyChange}/>
                        </div>
                    </div>
                    <div className="form-group-xs row-xs">
                        <div className="col-xs-12">
                            <div className="input-group">
                                <input type="text" name="spotDate" value={`${this.props.date} (Spot)`} className="form-control input-transparent" style={{width: "100%"}} disabled />
                                <span className="input-group-addon">
                                    <span className="fontello icon-fontello-calendar rubix-icon"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="row-xs">
                        <div className="col-xs-6">
                            <span className="spread-widget-spot-title">
                                Sell {this.state.currency}
                            </span>
                        </div>
                        <div className="col-xs-6 align-right">
                            <span className="spread-widget-spot-title">
                                Buy {this.state.currency}
                            </span>
                        </div>
                    </div>
                    <div className="form-group-xs row-xs relative">
                        <div className="col-xs-6">
                            <Spot className="spread-widget-spot" rate={this.props.bid}
                                  onClick={this.handleSpotClick({rate:this.props.bid})}/>
                        </div>
                        <div className="col-xs-6">
                            <Spot className="spread-widget-spot" rate={this.props.ask}
                                  onClick={this.handleSpotClick({rate:this.props.ask})}/>
                        </div>
                        <div className="spread-widget-spread-box centered">
                            {calcSpread(this.props.bid, this.props.ask)}
                        </div>
                    </div>
                </div>
                <ForwardTenors ref={(tenors)=>{this.tenors = tenors}} onQuote={this.getQuote}
                               tenors={this.props.tenors} onTenorsToggle={this.props.onTenorsToggle}/>
                <SpreadPending pending={this.state.pending}/>
            </div>
        );
    }
});
