import React from 'react';

import PureRenderMixin from 'react-addons-pure-render-mixin';

let confirmQuoteStyle = {
    width: "300px"
};
let titleStyle = {
    padding: "10px",
    fontSize: "18px",
    lineHeight: "18px",
    backgroundColor: "#2e507a",
    color: "#ffffff",
    fontWeight: "bold"
};
let bodyStyle = {
    padding: "0 10px 10px",
    fontSize: "14px"
};
let actionsStyle = {
    textAlign: "center"
};
let actionButtonStyle = {
    height: "25px",
    lineHeight: "25px",
    overflow: "hidden",
    margin: "10px 0",
    minWidth: 0,
    width: "100%",
    borderRadius: "5px"
};
let actionLabelStyle = {
    fontSize: "14px",
    textTransform: "none"
};
let expiredStyle = {
    fontWeight: "bold",
    textAlign: "center",
    color: "#910909",
    fontSize: "14px",
    margin: "0",
    padding: "15px"
};
let timerStyle={
    height: "50px"
};
let timerBarStyle = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) rotate(-90deg)",
}

let timerLabelStyle = {
    position: "absolute",
    top: "50%",
    left: "50%",
    fontWeight: "bold",
    color: "#2e507a",
    transform: "translate(-50%, -50%)",
}

let CircleTimer = React.createClass({
    propTypes: {
        min: React.PropTypes.number,
        max: React.PropTypes.number,
        size: React.PropTypes.number,
        onTimeIsUp: React.PropTypes.func
    },
    getInitialState(){
        return{
            value: this.props.max
        }
    },
    decreaseTimer(){
        this.setState({
            value: this.state.value - 1
        })
    },
    componentDidMount() {
        this.timerInterval = setInterval(()=>{
            if (this.state.value > 1){
                this.decreaseTimer();
            } else{
                clearInterval(this.timerInterval);
                this.decreaseTimer();
                if(this.props.onTimeIsUp){
                    this.props.onTimeIsUp();
                }
            }
        },1000)
    },
    componentWillUnmount(props) {
        clearInterval(this.timerInterval);
    },
    render() {
        return (
            <div className={this.props.className || null} style={timerStyle}>
                <div max={this.props.max} value={this.state.value} size={this.props.size} mode="determinate"
                                  style={timerBarStyle}/>
                <span style={timerLabelStyle}>{this.state.value}</span>
            </div>
        );
    }
});

export default React.createClass({
    propTypes: {
        quote: React.PropTypes.object,
        handleCancel: React.PropTypes.func,
        handleConfirm: React.PropTypes.func
    },
    getInitialState() {
        return {
            timeIsUp: false
        };
    },
    mixins:[PureRenderMixin],
    handleTimeIsUp(){
        this.setState({
            timeIsUp: true
        })
    },
    componentWillReceiveProps(props){
        if(this.props.quote !== props.quote){
            this.replaceState(this.getInitialState())
        }
    },

    render() {
        if (this.props.quote) {
            let ExpiringTimer = this.state.timeIsUp
                ? <p style={expiredStyle}>Quote Expired</p>
                : <CircleTimer size={0.75} max={this.props.quote.time} style={timerStyle}
                               className="confirm-quote-timer col-xs-12"
                               onTimeIsUp={this.handleTimeIsUp}/>

            return (
                <Dialog
                    modal={true}
                    open={this.props.quote? true : false}
                    onRequestClose={this.props.handleCancel}
                    title="Confirm Quote"
                    titleStyle={titleStyle}
                    bodyStyle={bodyStyle}
                    actionsContainerStyle={actionsStyle}
                    contentStyle={confirmQuoteStyle}
                    >
                    <div>
                        <span>ID:456287</span>

                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-xs-6 align-right">Counterparty:</div>
                                <div className="col-xs-6 align-center">{this.props.quote.company}</div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Buy/Sell:</div>
                                <div className="col-xs-6 align-center">Sell</div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Amount:</div>
                                <div className="col-xs-6 align-center">{this.props.quote.amount1}</div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Currency:</div>
                                <div className="col-xs-6 align-center">{this.props.quote.currency1}</div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Other Currency:</div>
                                <div className="col-xs-6 align-center">{this.props.quote.currency2}</div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Other Amount:</div>
                                <div className="col-xs-6 align-center">{this.props.quote.amount2}</div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Spot Rate:</div>
                                <div className="col-xs-6 align-center"><strong>{this.props.quote.rate}</strong></div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">Spot Date:</div>
                                <div className="col-xs-6 align-center">{this.props.quote.date}</div>
                            </div>
                            <div className="row">
                                {ExpiringTimer}
                            </div>
                            <div className="row">
                                <div className="col-xs-6 align-right">
                                    <RaisedButton
                                        label="Accept"
                                        primary={true}
                                        labelStyle={actionLabelStyle}
                                        style={actionButtonStyle}
                                        onTouchTap={this.props.handleConfirm}
                                        disabled={this.state.timeIsUp}
                                        />
                                </div>
                                <div className="col-xs-6 ">
                                    <RaisedButton
                                        label="Cancel"
                                        primary={false}
                                        labelStyle={actionLabelStyle}
                                        style={actionButtonStyle}
                                        onTouchTap={this.props.handleCancel}
                                        />
                                </div>
                            </div>
                        </div>
                    </div>
                </Dialog>
            )
        } else {
            return null;
        }
    }
});
