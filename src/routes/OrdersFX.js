/**
 * Created by Max Kudla.
 */

'use strict';

import React from 'react';
import Masonry from 'react-masonry-component';

import SpreadWidget from '../components/SpreadWidget';
import ConfirmQuoteDialog from '../components/ConfirmQuoteDialog';
import CounterPartySelect from '../components/CounterPartySelect';
import data from '../data/data.js';
import polling from '../data/polling.js'
import counterparty from '../data/counterparty.js';

export default React.createClass({
    getInitialState(){
        return {
            quote: null,
            counterparty: counterparty[0],
            data: data[counterparty[0].tier],
            pollingIndex: 0
        }
    },
    onGetQuote(data){
        console.log(data);

        return new Promise((resolve, reject)=> {

            //Here should be AJAX request
            let currencies = data.instrument.split("/");
            let currency2 = currencies[0] === data.currency ? currencies[1] : currencies[0];
            let ammount2 = data.tenor ? data.rate ? data.nominal / data.rate : data.nominal * data.rate : data.nominal / 1.22;

            setTimeout(()=> {
                this.setState({
                    quote: {
                        company: "Company A",
                        amount1: data.nominal,
                        currency1: data.currency,
                        amount2: Math.round(ammount2 * 100) / 100,
                        currency2: currency2,
                        rate: data.rate || 1.22,
                        date: "13/05/2015",
                        time: 15
                    }
                });
                resolve();
            }, 1500)
        })

    },
    handleCancelConfirm(){
        this.setState({
            quote: null
        })
    },
    handleAcceptConfirm(){
        console.log("Acctpted: ", this.state.quote);
        this.setState({
            quote: null
        })
    },
    handleCounterPartyChange(event, index, value){
        clearInterval(this.pollingIntrerval);
        this.setState({
            counterparty: value,
            data: data[value.tier],
            pollingIndex: 0
        }, function () {
            this.pollingIntrerval = setInterval(this.pollingFunction, 5000)
        })
    },
    pollingFunction(){
        if (this.state.pollingIndex < polling[this.state.counterparty.tier].length - 1) {
            this.setState({
                data: [].concat(polling[this.state.counterparty.tier][this.state.pollingIndex]),
                pollingIndex: this.state.pollingIndex + 1
            });
        } else {
            this.setState({
                data: [].concat(polling[this.state.counterparty.tier][this.state.pollingIndex]),
                pollingIndex: 0
            });
        }
    },
    componentDidMount(){
        this.pollingIntrerval = setInterval(this.pollingFunction, 5000);
    },
    render() {
        let WidgetsListLength = this.state.data.length;
        let WidgetsList = this.state.data.map((wdata, index)=> {
            return (
                <SpreadWidget onGetQuote={this.onGetQuote} key={`spread-widget-${this.state.counterparty.tier}-${wdata.instrument}`}
                              onTenorsToggle={()=>{this.masonry.forceUpdate()}} {...wdata}
                              style={{zIndex: WidgetsListLength - index}}/>
            )
        });

        return (
            <div className="content container">
                <h2 className="page-title">Orders FX page</h2>

                <div className="row-xs">
                    <div className="col-xs-3">
                        <CounterPartySelect counterParties={counterparty} value={this.state.counterparty}
                                            onChange={this.handleCounterPartyChange}/>
                    </div>

                    <div className="col-xs-3">
                        <input value={this.state.counterparty.tier} style={{width: "100%"}} name="tier"
                                   disabled/>
                    </div>
                    <div className="col-xs-3">
                        <input name="margin" style={{width: "100%"}} hintText="Margin"></input>
                    </div>
                </div>

                <div className="row">
                    <Masonry options={{transitionDuration: 0}} ref={(masonry)=>{this.masonry = masonry;}}>
                        {WidgetsList}
                    </Masonry>
                </div>
            </div>
        );
    }
});
