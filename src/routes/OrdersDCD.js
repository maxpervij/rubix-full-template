/**
 * Created by Max Kudla.
 */

'use strict';

import React from 'react';

export default React.createClass({
    render() {
        return (
            <div className="content container">
                <h1 className="page-title">Orders DCD page</h1>
            </div>
        );
    }
});
